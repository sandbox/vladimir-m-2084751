/**
 * @file
 * Main javascript file.
 */
(function ($) {
    Drupal.behaviors.usefulLinksMenuGrouping = {
        attach: function (context, settings) {
            var headerMenu = $('.block-useful-links .item-list ul');
            if (headerMenu.length) {
                if (!$('body').hasClass('utile-processed')) {
                    $('body').addClass('utile-processed');
                    var $pArr = headerMenu.find('li'),
                        pArrLen = $pArr.length,
                        pPerDiv = +$(".useful-links-block-columns-value").text() + 1;
                    for (var i = 0; i < pArrLen; i += pPerDiv) {
                        var wrapStr = '<div class="menu-item-wrapper wrapper-' + i + '" />';
                        $pArr.filter(':eq(' + i + '),:lt(' + (i + pPerDiv) + '):gt(' + i + ')')
                            .wrapAll(wrapStr);
                    }

                    var usefulLinks = $('.block-useful-links');
                    usefulLinks.find('.menu-item-wrapper')
                        .css('width', '130px');
                    usefulLinks.find('.item-list ul')
                        .css('width', $('.menu-item-wrapper').length * 180);
                    usefulLinks.find('.menu-item-wrapper')
                        .css('height', usefulLinks.find('.item-list ul').height() - 38);
                }
            }
        }
    }
})(jQuery);