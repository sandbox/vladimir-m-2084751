Install
=======
* In "/admin/modules" enable "useful_links" module.

Configuration
=============
* In "/admin/structure/block" configure "Useful links (useful_links)" block:
    - Set "Label";
    - Set "Select menu item to display";
    - Set "Number of elements to group by";
    - Set: Region, Pages, roles, Users etc.
* Save block.
